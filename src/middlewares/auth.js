module.exports = async (ctx, next) => {
  ctx.$user = await ctx.Users.findByChat(await ctx.getChat());
  await next();
};
