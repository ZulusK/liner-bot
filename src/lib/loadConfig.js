require('dotenv').config();
const Joi = require('joi');
const path = require('path');

const DEFAULT_JOI_OPTIONS = {
    allowUnknown: true,
    convert: true,
    presence: 'required'
};
const DEFAULT_CONFIG_FILENAME_PATTERN = 'config.#env';
/**
 * Loads configs from specified directory, and validates it by providing schema
 * @param {string} configDir - directory with config files
 * @param {object} validationSchema - schema for validation of configs
 * @param {object} options - custom options for Joi
 * @param {string} pattern - patter for names of config files
 */
module.exports = (configDir, validationSchema, options = {}, pattern = DEFAULT_CONFIG_FILENAME_PATTERN) => {
    const env = process.env.NODE_ENV || 'development';
    const configBasedOnEnv = require(path.join(configDir, pattern.replace('#env', env)));
    const { error, value: validatedConfig } = Joi.validate(configBasedOnEnv, validationSchema, Object.assign({ ...DEFAULT_JOI_OPTIONS }, options));
    if (error) {
        throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedConfig;
};