const fse = require('fs-extra');
const path = require('path');
const _ = require('lodash');
const MIGRATION_FILES_PATTERN = /(\d*)/;

async function getListOfNeededMigrations(dirname, options) {
  let files = await fse.readdir(dirname);
  files = files.filter(file => file.endsWith('.sql'));
  if (options.src) {
    files = _.filter(files, file => options.src.indexOf(file) >= 0);
  }
  files = _.filter(files, file => !options.last || options.last < MIGRATION_FILES_PATTERN.exec(file)[1]);
  return files.sort().map(file => path.join(dirname, file));
}

async function readAllFiles(files) {
  return Promise.all(files.map(file => fse.readFile(file)));
}

module.exports = async (knex, dirname, options) => {
  if (!path.isAbsolute(dirname)) throw new Error(`${dirname} is not an absolute path`);
  try {
    await fse.ensureDir(dirname);
  } catch (e) {
    throw new Error(`Directory ${dirname} is not exists`);
  }
  const sortedListOfMigrationsFiles = await getListOfNeededMigrations(dirname, options);
  console.log('Detected migration files:');
  sortedListOfMigrationsFiles.forEach(file => console.log(file));
  const filesContent = await readAllFiles(sortedListOfMigrationsFiles);
  console.log('Apply migrations');
  for (let i = 0; i < filesContent.length; i++) {
    console.log('Apply migration from file %s', sortedListOfMigrationsFiles[i]);
    await knex.raw(filesContent[i].toString('UTF8')); // eslint-disable-line no-await-in-loop
  }
};
