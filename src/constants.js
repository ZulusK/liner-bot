exports.scenes = {
  jobs: 'jobs',
  menu: 'start',
  policy: 'policy',
  jobCreation: 'jobCreation',
  jobDeletion: 'jobDeletion',
  queue: 'queue',
  queuePlaceCreation: 'queuePlaceCreation',
  gcIntegration: 'gcIntegration'
};


exports.commands = {
  enterJobsScene: '💼 работа',
  addJob: '➕ добавить должность',
  listJobs: '👁 должности',
  viewPolicy: '👁️ условия пользования',
  deleteJob: '❌ должность',
  back: '⬅️ назад',
  enterQueueScene: '⏳ очередь',
  addQueuePlace: '➕ добавить',
  listQueuePlaces: '👁 места в очередях',
  addGCKey: '📆 добавить Google Calendar'
};
