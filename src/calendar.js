const fse = require('fs-extra');
const { google } = require('googleapis');
const path = require('path');
const moment = require('moment');

const SCOPES = ['https://www.googleapis.com/auth/calendar'];

// Load client secrets from a local file.
fse
  .readFile(path.join(__dirname, '../credentials.json'))
  .then(content => {
    module.exports.credentials = JSON.parse(content);
  })
  .catch(err => console.log('Error loading client secret file:', err));

exports.getOAuth2Client = (auth) => {
  // eslint-disable-next-line camelcase
  const { client_secret, client_id, redirect_uris } = module.exports.credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
  if (auth) {
    oAuth2Client.setCredentials(auth);
  }
  return oAuth2Client;
};

exports.getAuthUrl = (oAuth2Client) => oAuth2Client.generateAuthUrl({
  access_type: 'offline',
  scope: SCOPES,
});

exports.getToken = (oAuth2Client, code) => oAuth2Client.getToken(code);

exports.listEvents = (auth) => {
  const calendar = google.calendar({ version: 'v3', auth });
  return calendar.events.list({
    calendarId: 'primary',
    timeMin: (new Date()).toISOString(),
    maxResults: 10,
    singleEvents: true,
    orderBy: 'startTime',
  }).then(data => data.items);
};

exports.addEvent = (event, auth) => {
  const calendar = google.calendar({ version: 'v3', auth });
  return calendar.events.insert({
    auth,
    calendarId: 'primary',
    resource: event,
  });
};
