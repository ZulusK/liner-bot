const constants = require('../constants');
const WizardScene = require('telegraf/scenes/wizard');
const Markup = require('telegraf/markup');
const extra = require('telegraf/extra');
const calendar = require('../calendar');

function askFollowLink(ctx) {
  const url = calendar.getAuthUrl(calendar.getOAuth2Client());
  ctx.reply('Шаг 1\nНажмите на кнопку и отправьте сюда код с открывшейся страницы',
    Markup.inlineKeyboard([
      Markup.urlButton('➡️ сюда', url)
    ]).extra());
  return ctx.wizard.next();
}

async function getTokens(ctx) {
  try {
    const tokens = await calendar.getToken(calendar.getOAuth2Client(), ctx.message.text);
    await ctx.$user.$query().patch({
      gcAccessToken: tokens.tokens.access_token,
      gcRefreshToken: tokens.tokens.refresh_token
    });
    ctx.reply('Вы успешно добавили ваш календарь в систему');
    ctx.reply('Теперь вы можете синхронизировать места в очереди с ним');
    return ctx.scene.enter(constants.scenes.menu);
  } catch (e) {
    ctx.reply('Вы ввели не верный код');
    return ctx.scene.enter(constants.scenes.menu);
  }
}

const steps = [
  askFollowLink,
  getTokens
];


class GCIntegrationScene extends WizardScene {
  static get name() {
    return constants.scenes.gcIntegration;
  }

  constructor() {
    super(GCIntegrationScene.name, ...steps);
  }
}

module.exports = GCIntegrationScene;
