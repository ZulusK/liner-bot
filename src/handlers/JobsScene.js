const Scene = require('telegraf/scenes/base');
const Markup = require('telegraf/markup');
const constants = require('../constants');
const extra = require('telegraf/extra');
const { enter, leave } = require('telegraf/stage');

function ask(ctx) {
  ctx.reply('Выберите действие', Markup.keyboard(
    [
      constants.commands.addJob,
      constants.commands.listJobs,
      constants.commands.deleteJob,
      constants.commands.back,

    ]
  ).oneTime().resize().extra());
}

function jobsToMD(jobs) {
  return jobs.map(job => job.toMD()).join('\n');
}

async function listJobs(ctx) {
  const jobs = await ctx.$user.$relatedQuery('jobs');
  ctx.reply(`Ваши должности:\n${jobsToMD(jobs)}`, extra.markdown());
}

class JobsScene extends Scene {
  static get name() {
    return constants.scenes.jobs;
  }

  constructor() {
    super(JobsScene.name);
    this.enter(ask);
    this.hears(constants.commands.addJob, enter(constants.scenes.jobCreation));
    this.hears(constants.commands.listJobs, listJobs);
    this.hears(constants.commands.deleteJob, enter(constants.scenes.jobDeletion));
    this.hears(constants.commands.back, enter(constants.scenes.menu));
  }
}

module.exports = JobsScene;
