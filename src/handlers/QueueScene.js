const Scene = require('telegraf/scenes/base');
const Markup = require('telegraf/markup');
const constants = require('../constants');
const extra = require('telegraf/extra');
const { enter, leave } = require('telegraf/stage');

function ask(ctx) {
  ctx.reply('Выберите действие', Markup.keyboard(
    [
      constants.commands.addQueuePlace,
      constants.commands.listQueuePlaces,
      constants.commands.back,
    ]
  ).oneTime().resize().extra());
}

function queuePlacesToMD(qps) {
  return qps.map(qp => qp.toMD()).join('\n');
}

async function listQueuesPlaces(ctx) {
  const qps = await ctx.$user.$relatedQuery('queuePlaces');
  ctx.reply(`Ваши места в очередях:\n${queuePlacesToMD(qps)}`, extra.markdown());
}

class QueueScene extends Scene {
  static get name() {
    return constants.scenes.queue;
  }

  constructor() {
    super(QueueScene.name);
    this.enter(ask);
    this.hears(constants.commands.addQueuePlace, enter(constants.scenes.queuePlaceCreation));
    this.hears(constants.commands.listQueuePlaces, listQueuesPlaces);
    this.hears(constants.commands.back, enter(constants.scenes.menu));
  }
}

module.exports = QueueScene;
