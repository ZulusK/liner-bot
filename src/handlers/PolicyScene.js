const Scene = require('telegraf/scenes/base');
const constants = require('../constants');
const extra = require('telegraf/extra');
const config = require('../../config');

const AGREE = { text: 'согласен', id: 'agree' };
const DISAGREE = { text: 'не согласен', id: 'diagree' };

function ask(ctx) {
  ctx.reply(
    `Если согласен с [условиями пользования](${config.privacyPolicy}), то нажми \`${AGREE.text}\`, иначе нажми \`${DISAGREE.text}\``,
    extra.markdown()
      .markup(m => m
        .inlineKeyboard(
          [AGREE, DISAGREE].map(t => m.callbackButton(t.text, t.id)),
          { columns: 3 }
        ))
  );
}

class PolicyScene extends Scene {
  static get name() {
    return constants.scenes.policy;
  }

  constructor(onAgree, onDisagree) {
    super(PolicyScene.name);
    this.enter(ask);
    this.action(AGREE.id, onAgree);
    this.action(DISAGREE.id, onDisagree);
  }
}

module.exports = PolicyScene;
