const constants = require('../constants');
const WizardScene = require('telegraf/scenes/wizard');
const extra = require('telegraf/extra');

const steps = [
  ctx => {
    ctx.reply([
      'Шаг 1',
      'Добавьте ваш ключ доступа'
    ].join('\n'));
    return ctx.wizard.next();
  },
  ctx => {
    if (ctx.message.text !== 'A') {
      ctx.reply('К сожалению ваш ключ недействителен, обратитесь в службу поддержки');
      return ctx.scene.enter(constants.scenes.jobs);
    } else {
      ctx.reply('Теперь отправьте вашу новую должность');
      return ctx.wizard.next();
    }
  },
  async ctx => {
    const jobTitle = ctx.message.text;
    const job = await ctx.Jobs.query().insertAndFetch({ userUid: ctx.$user.uid, title: jobTitle });
    ctx.reply(`Для вас создана новая должность ${job.toMD()}`, extra.markdown());
    return ctx.scene.enter(constants.scenes.jobs);
  }
];


class JobCreationScene extends WizardScene {
  static get name() {
    return constants.scenes.jobCreation;
  }

  constructor() {
    super(JobCreationScene.name, ...steps);
  }
}

module.exports = JobCreationScene;
