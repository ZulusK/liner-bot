CREATE TABLE IF NOT EXISTS "users"
(
  uid         VARCHAR(36)  NOT NULL PRIMARY KEY,
  first_name  VARCHAR(255),
  username    VARCHAR(255),
  last_name   VARCHAR(255),
  telegram_id VARCHAR(255) NOT NULL,
  created_at  DATE,
  updated_at   DATE,
  gc_access_token TEXT,
  gc_refresh_token TEXT
  );
CREATE UNIQUE INDEX users_uid_uindex ON public.users (uid);
CREATE TABLE IF NOT EXISTS jobs
(
  uid VARCHAR(36) PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  user_uid VARCHAR(36) NOT NULL,
  created_at  DATE,
  updated_at   DATE,
  CONSTRAINT professtion_users_uid_fk FOREIGN KEY (user_uid) REFERENCES public.users (uid)
);
CREATE UNIQUE INDEX jobs_uid_uindex ON public.jobs (uid);

CREATE TABLE public.queue_places
(
  uid VARCHAR(36) PRIMARY KEY,
  user_uid VARCHAR(36) NOT NULL,
  job_uid VARCHAR(36) NOT NULL,
  time TIMESTAMP NOT NULL,
  created_at DATE NOT NULL,
  updated_at DATE,
  CONSTRAINT queue_places_users_uid_fk FOREIGN KEY (user_uid) REFERENCES public.users (uid) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT queue_places_jobs_uid_fk FOREIGN KEY (job_uid) REFERENCES public.jobs (uid) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX queue_places_uid_uindex ON public.queue_places (uid);
