const { Model } = require('objection');
const { DbErrors } = require('objection-db-errors');

const moment = require('moment');

class BaseModel extends DbErrors(Model) {
  async $beforeInsert(queryContext) {
    this.createdAt = moment().format('YYYY/MM/DD HH:mm:ss');
    this.updatedAt = moment().format('YYYY/MM/DD HH:mm:ss');
    await super.$beforeInsert(queryContext);
  }
}

module.exports = BaseModel;
