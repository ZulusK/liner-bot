const path = require('path');

module.exports = {
  privacyPolicy: 'https://termsfeed.com/privacy-policy/32f36e757a460c7f00d0eeff668d473e',
  postgres: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    dialect: 'pg',
    pool: {
      min: 1,
      max: 10
    }
  },
  logger: {
    name: 'liner-bot',
    transports: [
      {
        type: 'console',
        level: 'debug'
      },
      {
        timestamps: true,
        type: 'file',
        level: 'info',
        filename: path.join(__dirname, '../logs/log.txt'),
        maxsize: 5 * 1024 ** 2,
        maxFiles: 5
      }
    ]
  },
  bot: {
    token: process.env.BOT_TOKEN
  },
  redis: {
    host: '127.0.0.1',
    port: 6379
  }
};
