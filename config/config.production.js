const path = require('path');

module.exports = {
    logger: {
        name: 'liner-bot',
        transports: [
            {
                type: 'console',
                level: 'info'
            },
            {
                timestamps: true,
                type: 'file',
                level: 'info',
                filename: path.join(__dirname, '../logs/log.txt'),
                maxsize: 10 * 1024 ** 2,
                maxFiles: 10
            }
        ]
    }
};