const Joi = require('joi');

const LoggerBaseJoiSchema = {
  colorize: Joi.boolean().default(false).optional(),
  json: Joi.boolean().default(false).optional(),
  level: Joi.string().valid('info', 'debug', 'error'),
  silent: Joi.boolean().optional()
};
const LoggerConsoleJoiSchema = Joi.object({
  type: Joi.string().valid('console'),
  ...LoggerBaseJoiSchema
});
const LoggerFileJoiSchema = Joi.object({
  type: Joi.string().valid('file'),
  filename: Joi.string(),
  maxsize: Joi.number().min(1024 /* 1 KB */).default(1024).optional(),
  maxFiles: Joi.number().min(1).default(1).optional(),
  ...LoggerBaseJoiSchema
});

const logger = Joi.object({
  timestamp: Joi.boolean().default(false).optional(),
  transports: Joi.array()
    .items(LoggerConsoleJoiSchema, LoggerFileJoiSchema)
    .default([LoggerConsoleJoiSchema])
    .min(1)
    .optional()
});

const postgres = Joi.object({
  host: Joi.string(),
  port: Joi.number(),
  user: Joi.string(),
  database: Joi.string(),
  password: Joi.string(),
  dialect: Joi.string().valid('pg', 'mysql', 'mysql2', 'sqlite3', 'mssql'),
  pool: Joi.object({
    min: Joi.number().optional().min(1),
    max: Joi.number().optional().min(1)
  }).optional()
});
const bot = Joi.object({
  token: Joi.string()
});
const redis = Joi.object({
  host: Joi.string(),
  port: Joi.number()
});
module.exports = {
  env: Joi.string().default(process.env.NODE_ENV).optional(),
  postgres,
  logger,
  bot,
  redis
};
